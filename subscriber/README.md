# Subscriber

Components in this category generally listen for some event and make a post from it. Your fridge is out of milk?
Post it to go-social automatically! This may also include more traditional things, like an API endpoint to just
send a post with some media.

All components here define an interface to the outside world and make posts from whatever data they get. They then
give those posts to publishers.
