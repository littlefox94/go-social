# Go-Social

Another fediverse social networking platform. But unlike the others,
this is completely cloud native

* Go!
* Microservice architecture!
* Docker!
* Kubernetes!
* Lot's of bugs!
* Code of conduct!
* Active for 3 months and forgotten!

Disclaimer: some of those are jokes, some are just added to make the *wrong* people laugh. And some are just fine.

People joining me would be great, I will do my best to be nice :)

## Repository layout

As written above, this software is built on a microservice architecture. This is the single repository for
the software, a so called "mono repository". In the root of the repository, you have rough categories of
services and inside those are the actual project directories for the single services.

The directory `lib` contains libraries, software that generates no binary but is integrated in other binaries.
Under that directory, the same kind of categorization is done again.

## Workflow

We follow the [Gitlab Workflow](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/) most of the time.

## CI/CD

This project will get a CI/CD config. We use Gitlabs builtin CI/CD solution, configured in the `.gitlab-ci.yml`.

Since computing power is still a bit expensive (at least for me), only staging and production is enabled for CD.
The staging environment will be available at https://test.0x0a.social, the prod environment at https://0x0a.social.

No code can be commited directly to `master` (securing the staging environment) and deploy to prod is only done manually.

## Why the heck another fediverse software?

Why not?

Seriously though, I like what mastodon achieved. I like the idea gnu social had before. And all the other softwares for
that problem. But I did not yet see a simple to install and extend software.

Yesterday (as of writing, the date I mean is 2018-12-18), I saw a thread on mastodon complaining about, why there
aren't a lot of new instances from hackerspaces. I wrote back "`sudo apt-get install gitlab-ce`, where is omnibus
mastodon?". I did not get an answer. But this question is the answer to "Why another": easy install for everyone
(using helm charts, since I think kubernetes is going to stay for a while) and easy extensions for developers
(swap a microservice, communicate via email).

Let's see where this goes.
