# Service registry

To make services not depending on specific implementations of other services, we need some kind of defined
interface for every kind of service. In this file, every kind of service is specified and all service implementations
in this repository are listed for every service type.

Service types are not the same thing as service categories. Service types are finer than categories. While a category
tells you, this service stores things, the type says what kind of thing (like `instance-config`).

The API specs for every service type can be found in the directory `api-specs`.

## Storage

### instance-config

Only one implementation of this is allowed per instance of go-social. Having multiple of those is technically possible,
but will not actually work. Maybe it will work by accident, but ... what do you thing will happen, when two components
know different things about the global configuration? No good things ...

#### Implementations

* instance-config-file

### posts

Stores posts. All their data and metadata, but may rely on other components to store parts of posts.

### media

Stores media, generally objects (files that are only written completely, without seeks).
