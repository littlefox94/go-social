package main

import (
	"flag"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/sirupsen/logrus"
	// "gitlab.com/littlefox94/go-social/api-spec/instance-config/v1alpha1"
	"gitlab.com/littlefox94/go-social/lib/cmdline"
)

type ConfigurationValue struct {
	tableName struct{} `sql:"instance_config"`

	Name  string `sql:"pk,type:varchar(255)"`
	Value string
}

func main() {
	cmdline.AddDefaultFlags()
	dbConnection := flag.String("db-connection", "postgres://gosocial:gosocial@localhost/gosocial", "Connection to database backend")

	flag.Parse()

	cmdline.UseDefaultFlags()

	if options, err := pg.ParseURL(*dbConnection); err != nil {
		logrus.WithError(err).Fatal("Cannot parse database connection info")
	} else {
		db := pg.Connect(options)

		if err := db.CreateTable(&ConfigurationValue{}, &orm.CreateTableOptions{
			IfNotExists: true,
		}); err != nil {
			logrus.WithError(err).Fatal("Cannot create table")
		} else {
			logrus.Info("All right, starting server")
		}
	}
}
