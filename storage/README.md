# Storage

Components that store things go here. There will most likely be a component storing posts in a database,
or a component storing user avatars. Maybe that one is the same as the one storing uploaded media, maybe not.

The components in this category store data in a defined format on hard disks (or SSDs or pingfs). They are
often hard to change because of that.
