# Code of Conduct for go-social, late 2018 version

Be excellent to each other.

No offending language on any medium where work on go-social is done. Not in changelogs,
not in code comments or issues. Not in the wiki, not on mailing lists not in Mattermost
and not in the fediverse. Nowhere. Mistakes happen, be nice and they will be forgiven.

Report problems to me directly or pick another contributor. My email address
is littlefox@lf-net.org. Contributors who got a message this way: handle with care, be
especially nice - the other person probably needs your help! Try to find a solution not
involving a ban. Ban if neccessary. Do not forward the message to other people without
permission of the original sender.

Happy coding for everyone!
