package cmdline

import (
	"flag"
	"github.com/sirupsen/logrus"
)

var logLevel *string

// Add some flags common to all executables
func AddDefaultFlags() {
	logLevel = flag.String("log-level", "warn", "Logging level (debug|info|warn|error|fatal)")
}

// Use the flags common to all executables
func UseDefaultFlags() {
	if level, err := logrus.ParseLevel(*logLevel); err != nil {
		logrus.WithError(err).Fatal("Could not parse log level")
	} else {
		logrus.SetLevel(level)
	}
}
