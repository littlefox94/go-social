module gitlab.com/littlefox94/go-social

require (
	github.com/go-pg/pg v7.0.0+incompatible
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/sirupsen/logrus v1.2.0
	mellium.im/sasl v0.2.1 // indirect
)
