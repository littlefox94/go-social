# API spec for storage/instance-config services

`TODO: transform into an OpenAPI 3 spec`

## Protocol

REST

### Endpoints

#### HEAD /v1alpha1/:key (required)

Should return the `Allowed-Methods` header as defined by the HTTP standard
to communicate if this implementation is able to change values.

#### GET /v1alpha1/:key (required)

Retrieve a single configuration value identified by the given key.

The value is returned in 2 possible ways:

* just the value if it is a scalar value
* json encoded value, if not a scalar value

The key is given with dots to use the hierarchic structure of the config.
When the instance config is the one below, the key `foo.bar` will return 42.

```json
{
    "foo": {
        "bar": 42
    }
}
```

#### PUT /v1alpha1/:key

Set a single configuration value identified by the given key.

Send the value in one of those ways:

* just the value if it's a scalar value
* json encoded if it's not a scalar value


## Configuration keys

Below is a YAML encoded full configuration file with example values.

```yaml
name:     0x0a.social
base_url: https://0x0a.social
```
