package v1alpha1

type InstanceConfig struct {
	Name    string `json:"name"`
	BaseURL string `json:"base_url"`
}
