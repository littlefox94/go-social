# UI

This category is for project end users actually see. Web interfaces, apps, design ideas,
artwork. Stuff like that.

It also includes backend code, that is only used by frontend stuff and not other backend code. Like
a mastodon-api to be compatible with mastodon frontends or generic REST api to be consumed by frontends.
